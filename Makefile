CONFIG_DIR=${HOME}/.config

all: install

install:
	@find . -maxdepth 1 -type d -not -name bash -not -name .git* -not -name . -not -name .images -exec rsync -av {} ${CONFIG_DIR}/ \;
