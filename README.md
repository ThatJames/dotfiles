# My dotfiles

Config files for the following services:
```
- picom
- i3-gaps
- nvim
- alacritty
- polybar
- rofi
```

# Installation

## Dependencies

- i3-gaps
- rofi
- picom
- nerd-fonts-complete
- neovim
- alacritty
- polybar

## Install using provided Makefile (requires the `make` tool)
```
git clone https://gitlab.com/thatjames/dotfiles.git
cd dotfiles
make
```

# Rofi Themes
Rofi themes shamelessly lifted from [here](https://github.com/adi1090x/rofi)

# Images

## Theme
![red and black theme overview](.images/plain_screen.png)

## Workspace

![workspace screen showing red-black colorscheme](.images/screen1.png)

## Application Menu

![application menu using rofi](.images/application_dmenu.png)

## Exit Menu

![rofi exit menu screenshot](.images/rofi.png)
